#import "ViewController.h"
#import <SpriteKit/SpriteKit.h>
#import "GameStartScene.h"

@interface ViewController ()

@end

@implementation ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	SKView *spriteView = (SKView *) self.view;
    spriteView.showsDrawCount = YES;
    spriteView.showsNodeCount = YES;
    spriteView.showsFPS = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    GameStartScene* gameStart = [[GameStartScene alloc] initWithSize:CGSizeMake(568,320)];
    SKView *spriteView = (SKView *) self.view;
    [spriteView presentScene:gameStart];
}



@end
