#import "GameStartScene.h"
#import "Level1Scene.h"

#define kGameTitle @"A Simple Labyrinth Game"
#define kGameTitleLabel @"gameTitleNode"

#define kStartTitle @"Start"
#define kStartButton @"startNode"
#define kStartText @"startText"

#define kFontName @"Helvetica"

@interface GameStartScene ()
@property BOOL contentCreated;
@end

@implementation GameStartScene

- (void)didMoveToView:(SKView *)view {
    if (!self.contentCreated) {
        [self createSceneContents];
        self.contentCreated = YES;
    }
}

- (void)createSceneContents {
    self.backgroundColor = [SKColor blueColor];
    self.scaleMode = SKSceneScaleModeAspectFit;
    [self addChild:[self createGameTitle]];
	[self addChild:[self createStartButton]];
}

#pragma mark sprite creation
- (SKLabelNode *)createGameTitle {
	SKLabelNode *gameTitle = [SKLabelNode labelNodeWithFontNamed:kFontName];
    gameTitle.text = kGameTitle;
    gameTitle.name = kGameTitleLabel;
    gameTitle.fontSize = 30;
    gameTitle.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
    return gameTitle;
}

- (SKSpriteNode *)createStartButton {
	SKSpriteNode *start = [SKSpriteNode spriteNodeWithColor:[SKColor orangeColor] size:CGSizeMake(200, 50)];
	SKLabelNode *label = (SKLabelNode*)[self childNodeWithName:kGameTitleLabel];
	start.position = CGPointMake(label.position.x,label.position.y - 50);
    start.name = kStartButton;
	
	SKLabelNode *startTitle = [SKLabelNode labelNodeWithFontNamed:kFontName];
    startTitle.text = kStartTitle;
    startTitle.name = kStartText;
    startTitle.fontSize = 24;
    startTitle.position = CGPointMake(0,-10);
	[start addChild:startTitle];
	
	return start;
}

#pragma mark UIResponder
- (void)touchesBegan:(NSSet *) touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];

    if ([node.name isEqualToString:kStartButton] || [node.name isEqualToString:kStartText]) {
		SKNode *start = [self childNodeWithName:kStartButton];
		if(start != nil) {
			start.name = nil;
			SKScene *level1  = [[Level1Scene alloc] initWithSize:self.size];
			SKTransition *transition = [SKTransition moveInWithDirection:SKTransitionDirectionLeft duration:0.5];
            [self.view presentScene:level1 transition:transition];
		}
    }
}


@end
