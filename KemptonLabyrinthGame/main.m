//
//  main.m
//  KemptonLabyrinthGame
//
//  Created by Justin Kempton on 4/7/14.
//  Copyright (c) 2014 Justin Kempton. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
