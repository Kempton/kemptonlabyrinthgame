#import "Level1Scene.h"
#import "GameStartScene.h"
#import <CoreMotion/CoreMotion.h>

#define kBoundry @"boundry"
#define kHole @"hole"
#define kBall @"ball.png"

@interface Level1Scene ()
@property BOOL contentCreated;
@property (nonatomic, strong) CMMotionManager *motionManager;
@property (nonatomic, strong) SKSpriteNode *ball;
@end

@implementation Level1Scene

static const uint32_t ballCategory = 0x1 << 0;
static const uint32_t holeCategory = 0x1 << 1;

- (void)didMoveToView:(SKView *)view {
    if (!self.contentCreated) {
		self.physicsWorld.contactDelegate = self;
        [self createSceneContents];
        self.contentCreated = YES;
        self.motionManager = [[CMMotionManager alloc] init];
        [self.motionManager startAccelerometerUpdates];
    }
}

- (void)createSceneContents {
	self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.scaleMode = SKSceneScaleModeAspectFit;
	
	[self addChild:[self createBackground]];
	[self addChild:[self createHole]];
	[self addChild:[self createBall]];
	
	SKSpriteNode *boundry;
	
	[self createBoundries:CGSizeMake(373, 5) name:@"piece1"];
	boundry = (SKSpriteNode*)[self childNodeWithName:@"piece1"];
	boundry.position = CGPointMake(boundry.size.width/2, 260);
	
	[self createBoundries:CGSizeMake(373, 5) name:@"piece2"];
	boundry = (SKSpriteNode*)[self childNodeWithName:@"piece2"];
	boundry.position = CGPointMake(boundry.size.width/2 + 65, 195);
	
	[self createBoundries:CGSizeMake(203, 5) name:@"piece3"];
	boundry = (SKSpriteNode*)[self childNodeWithName:@"piece3"];
	boundry.position = CGPointMake(boundry.size.width/2, 127);
	
	[self createBoundries:CGSizeMake(203, 5) name:@"piece4"];
	boundry = (SKSpriteNode*)[self childNodeWithName:@"piece4"];
	boundry.position = CGPointMake(boundry.size.width/2 + 65, 57);
	
	[self createBoundries:CGSizeMake(5, 141) name:@"piece5"];
	boundry = (SKSpriteNode*)[self childNodeWithName:@"piece5"];
	boundry.position = CGPointMake(270, 125);
	
	[self createBoundries:CGSizeMake(5, 130) name:@"piece6"];
	boundry = (SKSpriteNode*)[self childNodeWithName:@"piece6"];
	boundry.position = CGPointMake(355, boundry.size.height/2);
	
	[self createBoundries:CGSizeMake(5, 264) name:@"piece7"];
	boundry = (SKSpriteNode*)[self childNodeWithName:@"piece7"];
	boundry.position = CGPointMake(436, self.frame.size.height - boundry.size.height/2);
	
	[self createBoundries:CGSizeMake(5, 264) name:@"piece8"];
	boundry = (SKSpriteNode*)[self childNodeWithName:@"piece8"];
	boundry.position = CGPointMake(505, boundry.size.height/2);
	
}

#pragma mark sprite creation
- (SKSpriteNode *)createBall {
	SKTexture *texture = [SKTexture textureWithImageNamed:kBall];
	self.ball = [SKSpriteNode spriteNodeWithTexture:texture];
	self.ball.size = CGSizeMake(24, 24);
	self.ball.position = CGPointMake(self.ball.frame.size.width/2, self.frame.size.height - self.ball.frame.size.height/2);
	self.ball.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:12];
    self.ball.physicsBody.dynamic = YES;
    self.ball.physicsBody.affectedByGravity = NO;
    self.ball.physicsBody.mass = 0.02;
	self.ball.physicsBody.usesPreciseCollisionDetection = YES;
    self.ball.physicsBody.categoryBitMask = ballCategory;
	self.ball.physicsBody.contactTestBitMask = holeCategory;
	self.ball.physicsBody.collisionBitMask = 1;
	return self.ball;
}

- (SKSpriteNode *)createBackground {
	SKSpriteNode *background = [[SKSpriteNode alloc] initWithColor:[SKColor greenColor] size:CGSizeMake(568, 320)];
	background.position = CGPointMake(568/2, 320/2);
	return background;
}

- (SKSpriteNode *)createHole {
	//Would have preferred to use a shapenode here in the commented code below, but collisions were not working with shapenodes.
	//Just recycled the ball texture and changed blendfactor below to make it a black hole
	
//	CGRect box = CGRectMake(200, 200, 34, 34);
//	SKShapeNode *hole = [[SKShapeNode alloc] init];
//	hole.path = [UIBezierPath bezierPathWithOvalInRect:box].CGPath;
//	hole.fillColor = [SKColor yellowColor];
//	hole.strokeColor = [SKColor redColor];
//	hole.position = CGPointMake(0, 0);
//	hole.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:17];
//	hole.physicsBody.affectedByGravity = NO;
//  hole.physicsBody.dynamic = NO;
//	hole.physicsBody.usesPreciseCollisionDetection = YES;
//  hole.physicsBody.categoryBitMask = holeCategory;
//	hole.physicsBody.contactTestBitMask = ballCategory;
//	hole.physicsBody.collisionBitMask = 0;
	
	SKTexture *texture = [SKTexture textureWithImageNamed:kBall];
	SKSpriteNode *hole = [SKSpriteNode spriteNodeWithTexture:texture];
	hole.name = kHole;
	hole.color = [SKColor blackColor];
	hole.colorBlendFactor = 1.0;
	hole.position = CGPointMake((self.frame.size.width - hole.size.width/2) - 5, hole.size.height/2 + 3);
	hole.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:25];
	hole.physicsBody.dynamic = NO;
	hole.physicsBody.affectedByGravity = NO;
	hole.physicsBody.usesPreciseCollisionDetection = YES;
    hole.physicsBody.categoryBitMask = holeCategory;
	hole.physicsBody.contactTestBitMask = ballCategory;
	hole.physicsBody.collisionBitMask = 0;
	return hole;
}

- (void)createBoundries:(CGSize)size name:(NSString *)name {
	SKSpriteNode *boundry = [[SKSpriteNode alloc] initWithColor:[SKColor blackColor] size:CGSizeMake(size.width,size.height)];
    boundry.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:boundry.frame.size];
    boundry.physicsBody.dynamic = NO;
    boundry.physicsBody.affectedByGravity = NO;
	boundry.name = name;
	[self addChild:boundry];
}

#pragma mark accelerometer methods
- (void)update:(NSTimeInterval)currentTime {
    [self processUserMotionForUpdate:currentTime];
}

- (void)processUserMotionForUpdate:(NSTimeInterval)currentTime {
    CMAccelerometerData* data = self.motionManager.accelerometerData;
    if(fabs(data.acceleration.x) > 0.2  || fabs(data.acceleration.y) > 0.2  ) {
        [self.ball.physicsBody applyForce:CGVectorMake(20.0 * -data.acceleration.y, 20.0 * data.acceleration.x)];
    }
}

#pragma mark collision delegate
- (void)didBeginContact:(SKPhysicsContact *)contact {
    SKPhysicsBody *firstBody;
    SKPhysicsBody *secondBody;
	
    if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }else {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
	
	if(firstBody.categoryBitMask == ballCategory && secondBody.categoryBitMask == holeCategory) {
		self.ball.physicsBody = nil;
		SKNode *hole = [self childNodeWithName:kHole];
		SKAction *moveDropDown = [SKAction moveTo:CGPointMake(hole.position.x, hole.position.y) duration:0.5];
		SKAction *scaleDropX = [SKAction scaleXTo:0.0 duration:0.5];
		SKAction *scaleDropY = [SKAction scaleYTo:0.0 duration:0.5];
		SKAction *fadeDrop = [SKAction fadeAlphaTo:0.0 duration:0.8];
		SKAction *remove = [SKAction removeFromParent];
		SKAction *moveSequence = [SKAction group:@[moveDropDown, scaleDropX, scaleDropY, fadeDrop]];
		SKAction *allTogether = [SKAction sequence:@[moveSequence, remove]];
		[self.ball runAction: allTogether completion:^{
			self.motionManager = nil;
			GameStartScene* gameStart = [[GameStartScene alloc] initWithSize:self.size];
			SKTransition *transition = [SKTransition moveInWithDirection:SKTransitionDirectionRight duration:0.5];
			[self.view presentScene:gameStart transition:transition];
		}];
    }
}


@end
